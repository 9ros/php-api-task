<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Item;
use App\Repository\ItemRepository;
use App\Service\ItemService;
use App\Service\ParseDataService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends AbstractController
{
    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @Route("/item", name="item_list", methods={"GET"})
     * @IsGranted("ROLE_USER")
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->json([
                'error' => 'Invalid login request: check that the Content-Type header is "application/json".'
            ], 400);
        }

        return $this->json($this->itemRepository
            ->createQueryBuilder('item')
            ->getQuery()
            ->getArrayResult());
    }

    /**
     * @Route("/item", name="item_create", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param ItemService $itemService
     * @return JsonResponse
     */
    public function create(Request $request, ItemService $itemService): JsonResponse
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $data = $request->get('data');

        if (empty($data)) {
            return $this->json(['error' => 'No data parameter']);
        }

        $itemService->create($this->getUser(), $data);

        return $this->json([]);
    }

    /**
     * @Route("/item/{id}", name="items_delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @param int $id
     * @param ItemService $itemService
     * @return JsonResponse
     */
    public function delete(int $id, ItemService $itemService): JsonResponse
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $item = $this->itemRepository->findOneBy(['user' => $this->getUser(), 'id' => (int)$id]);

        if ($item === null) {
            return $this->json(['error' => 'No item'], Response::HTTP_BAD_REQUEST);
        }

        $itemService->delete($item);

        return $this->json([]);
    }

    /**
     * @Route("/item", name="item_update", methods={"PUT"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param ItemService $itemService
     * @param ParseDataService $parseDataService
     * @return JsonResponse
     */
    public function update(Request $request, ItemService $itemService, ParseDataService $parseDataService): JsonResponse
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if (empty($request->getContent())) {
            return $this->json(['error' => 'No data'], Response::HTTP_BAD_REQUEST);
        }

        $params = json_decode($request->getContent(), true);

        if (empty($params)) {
            $params = $parseDataService->parse_content_form_data($request->getContent());
        }

        if (!array_key_exists('data', $params) || !array_key_exists('id', $params)) {
            return $this->json(['error' => 'Check that the all parameters in request.'],
                Response::HTTP_BAD_REQUEST);
        }

        $id = (int)$params['id'];
        $data = $params['data'];
        if (empty($id) || empty($data)) {
            return $this->json(['error' => "Check parameter value. Params: id:'$id', data:'$data'"],
                Response::HTTP_BAD_REQUEST);
        }

        /** @var Item $item */
        $item = $this->itemRepository->findOneBy(['user' => $this->getUser(), 'id' => $id]);

        if ($item === null) {
            return $this->json(['error' => 'No item'], Response::HTTP_BAD_REQUEST);
        }

        $itemService->update($item, $data);

        return $this->json([]);
    }
}
