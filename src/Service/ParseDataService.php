<?php

namespace App\Service;


class ParseDataService
{
    /**
     * @param string $putData
     * @return array
     * parse data like:
     * "------WebKitFormBoundary\r\nContent-Disposition: form-data; name=\"id\"\r\n\r\n1\r\n
     * ------WebKitFormBoundary\r\nContent-Disposition: form-data; name=\"data\"\r\n\r\nnew item secret\r\n
     * ------WebKitFormBoundaryMxAqXolDfwBREREf--\r\n"
     */
    public function parse_content_form_data(string $putData): array
    {
        $putData = explode("name=\"", $putData);

        $parseData = [];
        if (!empty($putData)) {
            // remove not used element
            unset($putData[0]);
            // clear data
            foreach ($putData as $data) {
                $firstPattern = "\"\r\n\r\n";
                $secondPattern = "\r\n";

                $arrKey = explode($firstPattern, $data, 2);
                $key = $arrKey[0];

                $data = str_replace($arrKey[0] . $firstPattern, "", $data);

                $arrValue = explode($secondPattern, $data, 2);
                $value = $arrValue[0];

                $parseData[$key] = $value;
            }
        }
        return $parseData;
    }
} 