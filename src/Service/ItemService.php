<?php

namespace App\Service;

use App\Entity\Item;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ItemService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User|UserInterface $user
     * @param string $data
     */
    public function create($user, string $data): void
    {
        $item = new Item();
        $item->setUser($user);
        $item->setData($data);
        $user->addItem($item);

        $this->entityManager->persist($item);
        $this->entityManager->flush();
    }

    /**
     * @param Item $item
     * @param string $data
     */
    public function update(Item $item, string $data): void
    {
        $item->setData($data);
        $this->entityManager->flush();
    }

    /**
     * @param Item $item
     */
    public function delete(Item $item): void
    {
        $this->entityManager->remove($item);
        $this->entityManager->flush();
    }
} 