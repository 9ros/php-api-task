# Secure Information Storage REST API

### Project setup

* Add `secure-storage.localhost` to your `/etc/hosts`: `127.0.0.1 secure-storage.localhost`

* Run `make init` to initialize project

* Open in browser: http://secure-storage.localhost:8000/item Should get `Full authentication is required to access this resource.` error, because first you need to make `login` call (see `postman_collection.json` or `SecurityController` for more info).

### Run tests

make tests

### API credentials

* User: john
* Password: maxsecure

### Postman requests collection

You can import all available API calls to Postman using `postman_collection.json` file


# DATA

*API service builded on JSON Authentication Endpoint. After authentication, it becomes possible to receive items.*

### API login return

```
[
'username' => $this->getUsername(),
'roles' => $this->getRoles(),
]
```

### API requests for item

Get Items

```
  "request": {
    "method": "GET",
    "url": {
      "raw": "http://site/item",
    }
  },
  "response": []
```

Create Item
```
  "request": {
    "method": "POST",
    "url": {
      "raw": "http://site/item",
    }
    "body": {
        "mode": "formdata",
        "formdata": [
            {
                "key": "data",
                "value": "new item secret",
                "type": "text"
            }
        ]
    },
  },
  "response": []
```

Update Item
```
  "request": {
    "method": "PUT",
    "url": {
      "raw": "http://site/item",
    }
    "body": {
        "mode": "formdata",
        "formdata": [
          {
              "key": "id",
              "value": "1",
              "type": "text"
          },
          {
              "key": "data",
              "value": "new secret",
              "type": "text"
          }
      ]
    },
  },
  "response": []
```

Update Item
```
  "request": {
    "method": "POST",
    "url": {
      "raw": "http://site/item/1",
    }
  },
  "response": []
```