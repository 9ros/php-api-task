<?php

namespace App\Tests\Functional;

use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class ItemControllerTest extends WebTestCase
{
    public function testCreate()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByUsername('john');

        $client->loginUser($user);

        $data = 'very secure new item data';
        $newItemData = ['data' => $data];

        $client->request('POST', '/item', $newItemData);

        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString('', $client->getResponse()->getContent());
    }

    public function testDelete()
    {
        $client = static::createClient();

        $userRepository = static::$container->get(UserRepository::class);
        $itemRepository = static::$container->get(ItemRepository::class);

        $user = $userRepository->findOneByUsername('john');

        $client->loginUser($user);

        $data = 'very secure new item data';

        $itemToDelete = $itemRepository->findOneBy(['data' => $data], ['id' => 'DESC']);
        $client->request('DELETE', '/item/' . $itemToDelete->getId());

        $this->assertResponseIsSuccessful();
        $this->assertStringNotContainsString('very secure new item data', $client->getResponse()->getContent());
    }

    /**
     * @dataProvider urlProvider
     * @param $url
     */
    public function testGet($url)
    {
        $client = self::createClient();
        $userRepository = static::$container->get(UserRepository::class);

        $user = $userRepository->findOneByUsername('john');

        $client->loginUser($user);
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        yield ['/item'];
    }
}
